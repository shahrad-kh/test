# Generated by Django 4.1 on 2023-07-26 12:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0005_alter_news_tags'),
    ]

    operations = [
        migrations.AlterField(
            model_name='news',
            name='tags',
            field=models.ManyToManyField(default=None, to='blog.tag'),
        ),
    ]
